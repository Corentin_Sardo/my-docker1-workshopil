#!/usr/bin/env python
# -*- coding: utf-8 -*-

__name__ = 'wide_wild_west'
__version__ = '0.0.1.dev2'
__author__ = 'Xavier ROY'
__author_email__ = 'xavier.roy@telecomnnancy.net'

__import__('pkg_resources').declare_namespace(__name__)